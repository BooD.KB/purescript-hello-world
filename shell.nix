with import <nixpkgs> {};


mkShell {
  name = "PurescriptEnv";
  buildInputs = [
    purescript
    spago
    esbuild
    nodejs
  ];
}
